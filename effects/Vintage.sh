#!/bin/bash
convert -set colorspace RGB ../cam.jpg -channel R -level 33% \( -clone 0 -fill '#a159d3' -colorize 30% \) \( -clone 0 -colorspace gray -negate \) -compose blend -define compose:args=100,0 -composite ../DCIM/$(x="$(ls -v /home/pi/DCIM/| tail -n 1 | sed 's/.jpg//g')"; echo $x | awk '{printf $1 + 1}').jpg


