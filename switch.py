import RPi.GPIO as GPIO
import time
import subprocess
import os
import random

shell_list = ['/home/pi/effects/Gotham.sh', '/home/pi/effects/Lomo.sh', '/home/pi/effects/Nashville.sh', '/home/pi/effects/Redstone.sh', '/home/pi/effects/Toaster.sh', '/home/pi/effects/Vignette-1.sh', '/home/pi/effects/Vignette-2.sh', '/home/pi/effects/Vintage.sh']

GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
    input_state = GPIO.input(18)
    if input_state == False:
        subprocess.call(["raspistill","-n", "-w", "1251", "-h", "1039", "-ISO", "200", "-t", "1","-o", "cam.jpg"])
        os.system(random.choice(shell_list))
    time.sleep(0.2)
